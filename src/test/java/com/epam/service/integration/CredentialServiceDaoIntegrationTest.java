package com.epam.service.integration;

import com.epam.dao.CredentialDao;
import com.epam.dao.impl.CredentialDaoImpl;
import com.epam.model.entity.Credential;
import com.epam.service.CredentialService;
import com.epam.service.implementation.CredentialServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

public class CredentialServiceDaoIntegrationTest {
    private static CredentialDao credentialDao;
    private static CredentialService credentialService;


    @BeforeAll
    static void init() {
        credentialDao = new CredentialDaoImpl();
        credentialService = new CredentialServiceImpl();
    }

    @Test
    void testCreateAndFindByLogin() throws SQLException {
        Credential credential = new Credential("login", "password");
        credentialDao.create(credential);
        Credential credentialDaoInstance = credentialDao.findByLogin("login");
        Credential credentialServiceInstance = credentialService.findByLogin("login");
        Assertions.assertEquals(credentialDaoInstance,credentialServiceInstance);
    }
}
