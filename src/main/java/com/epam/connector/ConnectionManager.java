package com.epam.connector;

import com.epam.utility.PropertyLoader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
    private static Connection connection = null;

    private ConnectionManager() {
    }

    public static Connection getConnection() {
            if (connection == null) {
                try {
                    PropertyLoader propertyLoader = new PropertyLoader();
                    connection = DriverManager.getConnection(propertyLoader.getProperties().getProperty("url"),
                            propertyLoader.getProperties().getProperty("user"),
                            propertyLoader.getProperties().getProperty("password"));
                } catch (SQLException e) {
                    System.out.println("SQLException: " + e.getMessage());
                    System.out.println("SQLState: " + e.getSQLState());
                    System.out.println("VendorError: " + e.getErrorCode());
                }
            }

        return connection;
    }
}
