package com.epam.service.implementation;

import com.epam.dao.CredentialDao;
import com.epam.dao.impl.CredentialDaoImpl;
import com.epam.model.entity.Credential;
import com.epam.service.CredentialService;

import java.sql.SQLException;


public class CredentialServiceImpl implements CredentialService {

    private CredentialDao credentialDao;

    public CredentialServiceImpl() {
        this.credentialDao = new CredentialDaoImpl();
    }

    @Override
    public Credential findByLogin(String login) throws SQLException {
       return credentialDao.findByLogin(login);
    }

    @Override
    public int deleteById(Integer id) throws SQLException {
        return credentialDao.deleteById(id);
    }

    @Override
    public int update(Credential credential) throws SQLException{
      return credentialDao.update(credential);
    }

    @Override
    public int create(Credential credential) throws SQLException {
    return credentialDao.create(credential);
    }
}
