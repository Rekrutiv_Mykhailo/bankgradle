package com.epam.service.implementation;

import com.epam.dao.BankDao;
import com.epam.dao.impl.BankDaoImpl;
import com.epam.model.entity.Bank;

import java.sql.SQLException;
import java.util.List;

public class BankServiceImpl implements BankDao {
    private BankDao bankDao;

    public BankServiceImpl() {
        this.bankDao = new BankDaoImpl();
    }

    public Bank findByName(String name) throws SQLException {
        return bankDao.findByName(name);
    }

    @Override
    public Bank findById(Integer id) throws SQLException {
        return bankDao.findById(id);
    }

    @Override
    public int deleteById(Integer id) throws SQLException {
        return bankDao.deleteById(id);
    }

    @Override
    public int update(Bank bank) throws SQLException {
        return bankDao.update(bank);
    }

    @Override
    public List<Bank> findAll() throws SQLException {
        return bankDao.findAll();
    }

    @Override
    public int create(Bank bank) throws SQLException {
    return bankDao.create(bank);
    }


}
