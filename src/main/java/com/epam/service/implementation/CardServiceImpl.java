package com.epam.service.implementation;

import com.epam.dao.CardDao;
import com.epam.dao.impl.CardDaoImpl;
import com.epam.model.entity.Card;

import java.sql.SQLException;
import java.util.List;

public class CardServiceImpl implements CardDao {
    private CardDao cardDao;

    public CardServiceImpl() {
        this.cardDao = new CardDaoImpl();
    }

    @Override
    public List<Card> findByUser(Integer userId) throws SQLException {
        return cardDao.findByUser(userId);
    }

    @Override
    public List<Card> findByBank(Integer bankId) {
        return null;
    }

    @Override
    public Card findByID(Integer id) {
        return null;
    }

    @Override
    public int deleteById(Integer cardId) {
        return 0;
    }

    @Override
    public int create(Card card) throws SQLException {
        return 0;
    }

    @Override
    public int update(Card card) throws SQLException {
        return 0;
    }
}
