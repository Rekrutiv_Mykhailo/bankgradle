package com.epam.service;

import com.epam.model.entity.Credential;

import java.sql.SQLException;

public interface CredentialService {
    Credential findByLogin(String login) throws SQLException;
    int deleteById(Integer id) throws SQLException;
    int update(Credential credential) throws SQLException;
    int create(Credential credential) throws SQLException;
}
