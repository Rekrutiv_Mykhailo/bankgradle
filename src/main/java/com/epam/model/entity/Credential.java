package com.epam.model.entity;

import com.epam.model.Annotation.Column;
import com.epam.model.Annotation.Table;

import java.util.Objects;

@Table(name = "credential")
public class Credential {
    @Column(name = "id_credential")
    private Integer id;
    @Column(name = "login")
    private String login;
    @Column(name = "password")
    private String password;

    public Credential() {
    }

    public Credential(Integer id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
    }

    public Credential(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credential that = (Credential) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(login, that.login) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password);
    }

    @Override
    public String toString() {
        return "Credential{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
