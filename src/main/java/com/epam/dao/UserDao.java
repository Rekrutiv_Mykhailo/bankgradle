package com.epam.dao;

import com.epam.model.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDao {
    List<User> findAll() throws SQLException;
    User findByLogin(String login) throws SQLException;
    int update(User user)throws SQLException;
    int deleteById(Integer id)throws SQLException;
    int create(User user) throws SQLException;
}
