package com.epam.dao.impl;

import com.epam.connector.ConnectionManager;
import com.epam.dao.BankDao;
import com.epam.model.entity.Bank;
import com.epam.parser.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.utility.Utility.*;

public class BankDaoImpl implements BankDao {

    public Bank findByName(String name) throws SQLException {
        Bank bank = new Bank();
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_NAME_BANK)) {
            preparedStatement.setString(1, name);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    bank = (Bank) new Transformer(Bank.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return bank;
    }

    @Override
    public Bank findById(Integer id) throws SQLException {
        Bank bank = new Bank();
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_BANK)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    bank = (Bank) new Transformer(Bank.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return bank;
    }

    @Override
    public int deleteById(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE_BANK)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Bank bank) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE_BANK)) {
            fillPreparedStatement(bank, ps);
            ps.setInt(6, bank.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public List<Bank> findAll() throws SQLException {
        List<Bank> banks = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL_BANK)) {
                while (resultSet.next()) {
                    banks.add((Bank) new Transformer(Bank.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return banks;
    }

    @Override
    public int create(Bank bank) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE_BANK)) {
            fillPreparedStatement(bank, ps);
            return ps.executeUpdate();
        }
    }

    private void fillPreparedStatement(Bank bank, PreparedStatement ps) throws SQLException {
        ps.setString(1, bank.getName());
        ps.setInt(2, bank.getBalance());
        ps.setInt(3, bank.getTransactionPercent());
        ps.setInt(4, bank.getCurrency().getId());
        ps.setInt(5, bank.getCredential().getId());
    }

}
