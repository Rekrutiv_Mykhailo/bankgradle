package com.epam.dao.impl;

import com.epam.connector.ConnectionManager;
import com.epam.dao.CardDao;
import com.epam.model.entity.Card;
import com.epam.parser.Transformer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.epam.utility.Utility.FIND_BY_USER;

public class CardDaoImpl implements CardDao {

    @Override
    public List<Card> findByUser(Integer userId) throws SQLException {
        List<Card> cards = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_BY_USER)) {
                while (resultSet.next()) {cards.add((Card) new Transformer(Card.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return cards;   }

    @Override
    public List<Card> findByBank(Integer bankId) {
        return null;
    }

    @Override
    public Card findByID(Integer id) {
        return null;
    }

    @Override
    public int deleteById(Integer cardId) {
        return 0;
    }

    @Override
    public int create(Card card) throws SQLException {
        return 0;
    }

    @Override
    public int update(Card card) throws SQLException {
        return 0;
    }
}
