package com.epam.dao;

import com.epam.model.entity.Currency;

import java.sql.SQLException;
import java.util.List;

public interface CurrencyDao {
    Currency findByName(String name) throws SQLException;

    Currency findById(Integer id) throws SQLException;

    int deleteById(Integer id) throws SQLException;

    int update(Currency currency) throws SQLException;

    List<Currency> findAll() throws SQLException;
}
